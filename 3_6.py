import math
'''
For Problem 3.6 we assume the original key was a bitstring of 64 0 bits

and now the bit at position 1 has been flipped to a 1 bit.

In other words the key is: { 1_1, 0_2, 0_3, ..., 0_64 }, where each element means b_i and 1 <= i <= 64

PC - 1 places this 1 bit at position 8, or: { 0_1, 0_2, ..., 1_8, ..., 1_56 }, where each element means b_i and 1 <= i <= 64

Previously we discovered that since for Round 1 C_0 does a Left-Shift Rotate by 1 bit

meaning C_1 := { 0_1, ... , 0_7, ... , 0_28 }

the PC - 2 output places the 1 bit at the 20th bit position in the resulting 56-bit array.

This means the 4th S-box in Round 1 is affected based on the equation: ceiling( PC - 2 output bit position # / 6 )

The following program lists for which Round Numbers and which S-boxes are affected
'''


PC_2 = [
        14, 17, 11, 24, 1, 5,
        3, 28, 15, 6, 21, 10,
        23, 19, 12, 4, 26, 8,
        16, 7, 27, 20, 13, 2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 45, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29, 32
    ]


def find_bit_in_PC_minus_2(position):
    
    i = 0

    while i < len(PC_2):
        
        if PC_2[i] == position:

            return i + 1

        i += 1
    
    return None

C_i_bit_position = 7 # this is the bit position where the 1 bit is in C_1 after Round 1

PC_minus_2_i = 20 # this is the bit position where the 1 bit is in the 58-bit sequence after PC - 2 in Round 1

'''
Remember for Rounds: 1,2,9,16 there is a Left Shift Rotation of one bit

for all other Rounds there is a Left Shift Rotation of two bits
'''

rotate_one_bit_set = { 2, 9 , 16 }

round = 2

while round <= 16:
    
    if round in rotate_one_bit_set:

        # applying modulus 28 since C_i is a bit array of size 28

        C_i_bit_position = ( C_i_bit_position - 1 ) % 28

    else:
        
        C_i_bit_position = ( C_i_bit_position - 2 ) % 28

#   If C_i_bit_position is 0 then that means the bit position is 28

#   since bit positioning for this problem follows a 1-based index

    if C_i_bit_position == 0:

        C_i_bit_position = 28

    print("C_i_bit_position: " + str(C_i_bit_position) + "; ",end='')

    PC_minus_2_i = find_bit_in_PC_minus_2(C_i_bit_position)

    if PC_minus_2_i != None:

        print("Round " + str(round) + " ; S-box #: " + str(math.ceil(PC_minus_2_i/6)))
    
    else:
        print("Round " + str(round) + " ; No S-box affected")


    round += 1
