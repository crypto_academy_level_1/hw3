'''
The following program takes in a List of Numbers that represent the S-box output found

in Problem 3.3 and generates the correct Permutation P for DES based on Table 3.10
'''

S = [14,15,10,7,2,12,4,13] # each number is a 4-bit output in the Python List

S_bitarray = [None] * 32 # stores the bitstring form of S

i = 0

while i < 32:
    
    S_bitarray[i] = ( S[i//4] >> (4 - (i % 4) -1) ) & 0b1

    print("S_bitarray[" + str(i) + "]: " + str(S_bitarray[i]) + ",",end='')

    i += 1

    if i % 4 == 0 and i != 0:

        print('')

P_table = [16,7,20,21,29,12,28,17,1,15,23,26,5,18,31,10,2,8,24,14,32,27,3,9,19,13,30,6,22,11,4,25]

i = 1

P = [None] * 32

while i <= 32:

    P_bit = P_table[i-1]

    P[i-1] = S_bitarray[P_bit-1]

#    P[i-1] = ( S[P_bit//4 - 1] >> ( 4 - ( (P_bit-1) % 4) - 1 ) ) & 0b1

    print("P[" + str(i) + "]: " + str(P[i-1]) + ", ",end='')

    if i % 4 == 0:
        print('')

    i += 1


